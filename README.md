# tcp-chatservice

A small python3 application (just!) for educational use.

## How to use it

(coming soon)

## Bugs

- [ ] #2
- [ ] #1 Socket is blocked after running. Maybe [Stack Overflow](https://stackoverflow.com/questions/26313182/python-with-as-statement-and-multiple-return-values) helps.

## Feature Requests

- [ ] #6
- [ ] #5 Create devel branch.
- [ ] #4 Add an interactive mode for the client so that the user can write multiple messages.
- [ ] #3 Add a licence to this software so that it is not legal to include it into proprietary software. The text after 'ENDE DER LIZENZBEDINGUNGEN' on [this website](http://www.gnu.de/documents/gpl.de.html) should help to get a GPL licence. Also add the feature, that the licence is printed with the recommended command.
- [ ] #2 Implement a .json address book instead of the hard-coded python dictionary 'user'.
- [ ] #1 Write the text for the 'How to use it' section of the README.md (from 'git clone' to running code).

## Unnoetiger kommentar von Marco
- [ ] Hallo Moin

##TESTTESTEST
- [ ] help...

## Unnoetiger kommentar von Tom
- [ ] Le Crossaint 

