#!/usr/bin/python3


import socket
import json


HOST = "127.0.0.1" # socket.gethostname()
PORT = 5008
MAX_REQUESTS = 10
MAX_BYTES_READ = 1024


serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind((HOST, PORT))
serversocket.listen(MAX_REQUESTS)

with open("addressbook.json", "r") as file_addressbook:

    user = json.load(file_addressbook)
    print(user)


while True:

    print("try to accept..")
    clientsocket, addr = serversocket.accept()

    print('Got a connection from %s' % str(addr))

    msg = clientsocket.recv(MAX_BYTES_READ)
    ip = str(addr[0])
    if ip in user:
        print("From: " + user[ip] + " '" + msg.decode("utf-8") + "'")
    else:
        print("From: " + ip + " '" + msg.decode("utf-8") + "'")

    msg = "Thank you for connecting" + "\r\n"
    clientsocket.send(msg.encode('utf-8'))
    clientsocket.close()
